include($$PWD/../../dep/qmake/common.pri)

QT -= gui

TEMPLATE = lib

CONFIG += c++11

contains(DEFINES, STATIC_LIBS_BUILD) {
    CONFIG   += staticlib
}
else {
    CONFIG   += lib
}
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QCTRLSIGNALS_LIBRARY
# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += qctrlsignalhandler.h \
           qctrlsignal_global.h \
           qctrlsignalhandler_p.h

SOURCES += qctrlsignalhandler.cpp

win32 {
        HEADERS += qctrlsignalhandler_win.h
        SOURCES += qctrlsignalhandler_win.cpp
}

unix {
        HEADERS += qctrlsignalhandler_unix.h
        SOURCES += qctrlsignalhandler_unix.cpp
}
# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    qctrlsignals.pri
