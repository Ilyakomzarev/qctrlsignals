INCLUDEPATH += $$PWD

DEFINES += QCTRLSIGNALS_INCLUDE_SOURCE

HEADERS += $$PWD/qctrlsignalhandler.h \
           $$PWD/qctrlsignal_global.h \
           $$PWD/qctrlsignalhandler_p.h

SOURCES += $$PWD/qctrlsignalhandler.cpp

win32 {
        HEADERS += $$PWD/qctrlsignalhandler_win.h
        SOURCES += $$PWD/qctrlsignalhandler_win.cpp
}

unix {
        HEADERS += $$PWD/qctrlsignalhandler_unix.h
        SOURCES += $$PWD/qctrlsignalhandler_unix.cpp
}
